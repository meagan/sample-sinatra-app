# Sinatra Application

This is a sample Sinatra application for my 8th Light Apprenticeship.

This application provides basic form interaction. 

Enter text into form, press enter and your reversed input will show up.

Check out the app [here](http://sample-reverse-it.herokuapp.com/)

